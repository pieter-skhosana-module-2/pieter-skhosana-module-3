// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'dashboard.dart';
import 'register.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //bar on the login page
      appBar: AppBar(
        title: const Text("Login"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          //input fields for username and password
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(15),
              child: TextField(
                decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Username",
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(15),
              child: TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Password",
                ),
              ),
            ),
            //Login button
            const SizedBox(height: 50),
            ElevatedButton(
              onPressed: () => {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => const Dashboard()))
              },
              child: const Text("Login"),
              style: ElevatedButton.styleFrom(
                fixedSize: const Size(300, 50),
              ),
            ),
            //register button
            const SizedBox(height: 50),
            ElevatedButton(
              onPressed: () => {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => const Register()))
              },
              child: const Text("No Account? Register"),
              style: ElevatedButton.styleFrom(
                fixedSize: const Size(300, 50),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
