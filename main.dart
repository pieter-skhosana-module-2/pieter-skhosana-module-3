import 'package:flutter/material.dart';
import 'login.dart';

void main() {
  runApp(const Main());
}

/*  
* upon execution the user will be taken to the login screen
* if an account exist, they will login
* they need to click register if no account exists
* 
*/
class Main extends StatelessWidget {
  const Main({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Module_3 App',
      home: const Login(),
      theme: ThemeData(
          primarySwatch: Colors.teal,
          accentColor: Colors.deepPurple,
          scaffoldBackgroundColor: Colors.white),
    );
  }
}
