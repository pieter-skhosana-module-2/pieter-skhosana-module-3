import 'package:flutter/material.dart';
import 'login.dart';

/*
A user to register using only username and password
The user will the be taken back to the login page
the back button will be diabled
*/
class Register extends StatelessWidget {
  const Register({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register"),
        centerTitle: true,
      ),
      body: Column(children: <Widget>[
        const Padding(
          padding: EdgeInsets.all(15),
          child: TextField(
            decoration: InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Username",
              hintText: "Enter Your name",
            ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.all(15),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Password",
            ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.all(15),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Confirm Password",
            ),
          ),
        ),
        const SizedBox(
          height: 50,
        ),
        ElevatedButton(
          onPressed: () => {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => const Login()))
          },
          child: const Text("Register"),
          style: ElevatedButton.styleFrom(
            fixedSize: const Size(300, 50),
          ),
        ),
      ]),
    );
  }
}
