import 'package:flutter/material.dart';

class TEMPERATURE extends StatelessWidget {
  const TEMPERATURE({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Convert Celcius to Fahrenheit"),
        centerTitle: true,
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          const Padding(
            //text input field for the temperature reading
            padding: EdgeInsets.all(15),
            child: TextField(
              decoration: InputDecoration(
                border: UnderlineInputBorder(),
                label: Text("Temperature (C)"),
                hintText: "Enter reading in Celcius",
              ),
            ),
          ),
          //button to convert celcius to fahrenheit
          const SizedBox(height: 50),
          ElevatedButton(
            onPressed: () => {},
            child: const Text("Convert"),
            style: ElevatedButton.styleFrom(
              fixedSize: const Size(300, 50),
            ),
          ),
        ],
      )),
    );
  }
}
